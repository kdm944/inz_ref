var should = require('should'),
    Mod    = require('../../src/js/salbp1');

let problem = () => {
  return {
    operations: [
      { id: 1, time: 2 },
      { id: 2, time: 1, preds: [1] },
      { id: 3, time: 4, preds: [2] },
      { id: 4, time: 3, preds: [1] },
      { id: 5, time: 2, preds: [3, 4] }
    ],

    cycle: 5
  };
};

let emptySolution = () => {
  return {
    weights: [],
    stations: [],
    indices: {}
  };
};

let rpwWeights = () => {
  return [
    { id: 1, weight: 9 },
    { id: 2, weight: 7 },
    { id: 3, weight: 6 },
    { id: 4, weight: 5 },
    { id: 5, weight: 2 }
  ];
};

describe('Salbp1', () => {

  var prob, sol;

  beforeEach( () => {
    prob = problem();
  });

  it("First decision - RPW weights", () => {
    let decisions = Mod.decisions(prob, emptySolution());

    decisions.should.be.an.Array();
    decisions = decisions.filter( (d) => d.method === 'rpw' );

    decisions.should.be.deepEqual([
      { type: 'weights', method: 'rpw',
        values: rpwWeights() }
    ]);

    // the problem should remain unchanged!
    prob.should.be.deepEqual( problem() );
  });

  let deepCheck = (decisions, obj) => {
    decisions.should.be.an.Array();
    decisions.should.be.deepEqual([ obj ]);
  };

  let containCheck = (decisions, arr) => {
    decisions.should.be.an.Array();
    decisions.should.containDeep( arr );
  };

  let generalTest = (spec) =>
    it(spec.title, () => {
      sol = emptySolution();
      sol.weights = spec.weights_func();
      sol.stations = spec.initial_stations;

      let decisions = Mod.decisions(prob, sol);

      spec.decisions.forEach( (d) => containCheck(decisions, [d]) );
      decisions.forEach( (d) => containCheck(spec.decisions, [d]) );

      prob.should.be.deepEqual( problem() );
    });

  let opDec = (id) => {
    return {
      type: 'operation', id
    };
  };

  let sch = (id, start, end) => {
    return {
      id, start, end
    };
  };

  generalTest({
    title:              'RPW, 1st operation',
    weights_func:       rpwWeights,
    initial_stations:   [[]],
    decisions:          [ opDec(1) ]
  });

  generalTest({
    title:              'RPW, 2nd operation',
    weights_func:       rpwWeights,
    initial_stations:   [ [ sch(1,0,2) ] ],
    decisions:          [ opDec(2) ]
  });

  generalTest({
    title:              'RPW, after 2nd operation',
    weights_func:       rpwWeights,
    initial_stations:   [ [ sch(1,0,2), sch(2,2,3) ] ],
    decisions:          [ { type: 'new-station' } ]
  });

  generalTest({
    title:              'RPW, 3rd operation',
    weights_func:       rpwWeights,
    initial_stations:   [ [ sch(1,0,2), sch(2,2,3) ],
                          [] ],
    decisions:          [ opDec(3) ]
  });

  generalTest({
    title:              'RPW, after 3rd operation',
    weights_func:       rpwWeights,
    initial_stations:   [ [ sch(1,0,2), sch(2,2,3) ],
                          [ sch(3,0,4) ] ],
    decisions:          [ { type: 'new-station' } ]
  });

  generalTest({
    title:              'RPW, 4th operation',
    weights_func:       rpwWeights,
    initial_stations:   [ [ sch(1,0,2), sch(2,2,3) ],
                          [ sch(3,0,4) ],
                          [] ],
    decisions:          [ opDec(4) ]
  });
  
  generalTest({
    title:              'RPW, 5th operation',
    weights_func:       rpwWeights,
    initial_stations:   [ [ sch(1,0,2), sch(2,2,3) ],
                          [ sch(3,0,4) ],
                          [ sch(4,0,3) ] ],
    decisions:          [ opDec(5) ]
  });

  let indicesTest = (title, decs, indices = []) =>
    it(title, () => {
      sol = emptySolution();
      sol.weights = rpwWeights();
      sol.stations = [ 
        [ sch(1,0,2),
          sch(2,2,3) ],
        [ sch(3,0,4) ],
        [ sch(4,0,3),
          sch(5,3,5) ]
      ];
      indices.forEach( (i) => sol.indices[i.type] = i.value );

      let decisions = Mod.decisions(prob, sol);

      containCheck(decisions, decs);

      // the problem should remain unchanged!
      prob.should.be.deepEqual( problem() );
    });

  indicesTest('RPW, all indices', [
    { type: 'le', value: 80 },
    { type: 'si', value: 2.24 },
    { type: 'lt', value: 15 }
  ]);

  indicesTest('RPW, le known', [
    { type: 'si', value: 2.24 },
    { type: 'lt', value: 15 }
  ], [
    { type: 'le', value: 80 }
  ]);

  indicesTest('RPW, le, si known', [
    { type: 'lt', value: 15 }
  ], [
    { type: 'le', value: 80 },
    { type: 'si', value: 2.24 }
  ]);

  it('RPW, complete solution', () => {
    sol = emptySolution();
    sol.weights = rpwWeights();
    sol.stations = [ 
      [ sch(1,0,2),
        sch(2,2,3) ],
      [ sch(3,0,4) ],
      [ sch(4,0,3),
        sch(5,3,5) ]
    ];
    sol.indices = {
      le: 80,
      si: 2.24,
      lt: 15
    };

    let decisions = Mod.decisions(prob, sol);

    containCheck(decisions, [ { type: 'nothing-to-do' } ]);

    // the problem should remain unchanged!
    prob.should.be.deepEqual( problem() );
  });
  
  // when the weights are equal...  
  it("The same weights, 2nd operation", () => {
    sol = emptySolution();
    sol.weights = rpwWeights();
    sol.weights.filter( (w) => w.id === 4 )[0].weight = 7;
    sol.stations = [ 
      [ { id: 1, start: 0, end: 2 } ] 
    ];

    let decisions = Mod.decisions(prob, sol);

    containCheck(decisions, [
      { type: 'operation', id: 2 },
      { type: 'operation', id: 4 }
    ]);

    // the problem should remain unchanged!
    prob.should.be.deepEqual( problem() );
  });

  // and when the station time is not long enough
  it("Operations allowed due to station time", () => {
    sol = emptySolution();
    sol.weights = rpwWeights();
    sol.weights.filter( (w) => w.id === 4 )[0].weight = 7;
    sol.stations = [ 
      [ { id: 1, start: 0, end: 2 } ] 
    ];
    prob.operations.filter( (o) => o.id === 4 )[0].time = 4;

    let decisions = Mod.decisions(prob, sol);

    containCheck(decisions, [
      { type: 'operation', id: 2 }
    ]);

    // the problem should remain unchanged!
    let p = problem();
    p.operations.filter( (o) => o.id === 4 )[0].time = 4;
    prob.should.be.deepEqual( p );
  });

});